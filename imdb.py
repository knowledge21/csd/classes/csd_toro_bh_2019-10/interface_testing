import os
from unittest import TestCase

from selenium import webdriver


class TestIMDB(TestCase):

    def setUp(self) -> None:
        self.browser = webdriver.Chrome(os.getcwd() + '/chromedriver')

    def test_verifica_se_o_diretor_de_o_som_ao_redor_e_o_Kleber_Mendonca_Filho(self):
        nome_site = 'https://www.imdb.com/'
        nome_filme = "O som ao redor"
        nome_diretor_esperado = "Kleber Mendonça Filho"

        self.browser.get(nome_site)
        navbar = self.browser.find_element_by_css_selector("#navbar-query")
        navbar.send_keys(nome_filme)
        self.browser.find_element_by_css_selector("#navbar-submit-button").click()
        self.browser.find_element_by_css_selector("#main > div > div.findSection > table > tbody > tr > td.result_text > a").click()
        nome_diretor_observado = self.browser.find_element_by_css_selector("#title-overview-widget > div.plot_summary_wrapper > div.plot_summary > div:nth-child(2) > a").text

        self.assertEqual(nome_diretor_esperado, nome_diretor_observado)