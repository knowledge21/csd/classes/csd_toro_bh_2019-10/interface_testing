import os
from time import sleep
from unittest import TestCase

from selenium import webdriver

class TestToro(TestCase):

    def setUp(self) -> None:
        self.browser = webdriver.Chrome(os.getcwd() + '/chromedriver')

    def test_verifica_ativo_bolsa(self):
        nome_site = 'https://www.toroinvestimentos.com.br/'
        nome_ativo = "KNRI11"

        self.browser.get(nome_site)
        sleep(1)
        self.browser.find_element_by_css_selector(".nav-burguer").click()
        sleep(1)
        self.browser.find_element_by_xpath("//*[@class='tr-side-menu-wrapper']//child::a[text()='Bolsa de Valores']").click()
        sleep(1)
        self.browser.find_element_by_xpath("//*[@id='global-wrapper'] //*[contains(text(),'Buscar')]").click()
        sleep(1)
        self.browser.find_element_by_xpath("//*[@id='global-wrapper']//hub-search/div/div[1]/div/div/div/div/div[1]/input").send_keys("KNRI11")
        sleep(2)
        self.browser.find_element_by_xpath("//*[@id='global-wrapper'] //p[contains(text(),'KNRI11')]").click()
        sleep(1)
        self.assertEqual(nome_diretor_esperado, nome_diretor_observado)